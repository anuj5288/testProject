import json
import random
from PIL import Image
from pprint import pprint

config_file = 'Config.json'
filepath='/data/images'
crop_center = True
crop_largest_rect = True

def parseJson():
	with open(config_file) as f:
		data = json.load(f)
	
	return data

			

def crop_around_center(image, width, height):
    """
    Given a NumPy / OpenCV 2 image, crops it to the given width and height,
    around it's centre point
    Source: http://stackoverflow.com/questions/16702966/rotate-image-and-crop-out-black-borders
    """

    image_size = (image.shape[1], image.shape[0])
    image_center = (int(image_size[0] * 0.5), int(image_size[1] * 0.5))

    if(width > image_size[0]):
        width = image_size[0]

    if(height > image_size[1]):
        height = image_size[1]

    x1 = int(image_center[0] - width * 0.5)
    x2 = int(image_center[0] + width * 0.5)
    y1 = int(image_center[1] - height * 0.5)
    y2 = int(image_center[1] + height * 0.5)

    return image[y1:y2, x1:x2]	
	


def crop_largest_rectangle(image, angle, height, width):
    """
    Crop around the center the largest possible rectangle
    found with largest_rotated_rect.
    """
    return crop_around_center(
        image,
        *largest_rotated_rect(
            width,
            height,
            math.radians(angle)
        )
    )

def generate_rotated_image(image, angle, size=None, crop_center=False,
                           crop_largest_rect=False):
    height, width = image.shape[:2]
    if crop_center:
        if width < height:
            height = width
        else:
            width = height

    image = rotate(image, angle)

    if crop_largest_rect:
        image = crop_largest_rectangle(image, angle, height, width)

    if size:
        image = cv2.resize(image, size)

    return image

	
def generate_Images(data, type, output_path):
	start_1 = data[type][0]["start"]
	end_1 = data[type][1]["end"]
	
	start_2 = data[type][0]["start"]
	end_2= data[type][1]["end"]
	
	straight_path = data[output_path]
	
	for file in os.listdir(filepath):
		imagePath = filepath + '/' + file
		#Generate 10 random images
		for x in range(10):
			rotation_angle =  random.randint(start_1,end_1);
			rotated_image = generate_rotated_image(
                image,
                rotation_angle,
                size=self.input_shape[:2],
                crop_center=crop_center,
                crop_largest_rect=crop_largest_rect
            )
			
			out_fp=straight_path+ '/' + rotated_image
			rotated_image.save(out_fp)
		
    	#Generate next 10 random images
    	for x in range(10):
    		rotation_angle =  random.randint(start_2,end_2);
    		rotated_image = generate_rotated_image(
                image,
                rotation_angle,
                size=self.input_shape[:2],
                crop_center=crop_center,
                crop_largest_rect=crop_largest_rect
            )
    		
			out_fp=straight_path+ '/' + rotated_image
			rotated_image.save(out_fp)
		
				
def main():
	data = parseJson();
	generate_Images(data, "Straight", "straight_out_path");
	generate_Images(data, "Tilted", "tilted_out_path");
	generate_Images(data, "Inverted", "inverted_out_path");
